/**
 * This class represents the quad tree nodes.
 *
 */
public class Rectangle {
	//Declaring variables to define and get the rectangle properties
	float x, y; // x and y positions of rectangle
	float breadth, length; //height and width of a rectangle
	Point location; //Positions of Point
	int quadrant; //location of point in 2D plane 
	/*
	 * Constructor to initialize Rectangle Object
	 */
	public Rectangle() {

	}
	/*
	 * Constructor to define a new rectangular object with the passed parameters
	 * @param X - coordinate of a rectangle in X plane
	 * @param Y - coordinate of a rectangle in Y plane
	 * @param breadth - width of a rectangle
	 * @param length - height of a rectangle
	 */
	public Rectangle(float x, float y, float breadth, float length) {
		this.x = x; 
		this.y = y;
		this.breadth = breadth; 
		this.length = length; 
	}
	/*
	 * Constructor to define a new rectangular object with the passed parameters
	 * @param X - coordinate of a rectangle in X plane
	 * @param Y - coordinate of a rectangle in Y plane
	 * @param breadth - width of a rectangle
	 * @param length - height of a rectangle
	 * @param quadrant - quadrant of a rectangle in the plane
	 */
	public Rectangle(float x, float y, float breadth, float length, int quadrant) {
		this.x = x; 
		this.y = y;
		this.breadth = breadth; 
		this.length = length; 
		this.quadrant = quadrant;
	}
	/*
	 * Constructor to define a new rectangular object with the passed parameters
	 * @param Point - point object containing X,Y position of a point
	 * @param breadth - width of a rectangle
	 * @param length - height of a rectangle
	 * @param quadrant - quadrant of a rectangle in the plane
	 */
	public Rectangle(Point p, float breadth, float length, int quadrant) {
		this.x = p.x_axis; 
		this.y = p.y_axis;
		this.breadth = breadth; 
		this.length = length; 
		this.quadrant = quadrant;
	}
    /*
     * Constructor to initialize the rectangle shape
     * @param rect - rectangle object 
     */
	public Rectangle(Rectangle rect) {
		this.x = rect.x; 
		this.y = rect.y;
		this.breadth = rect.breadth; 
		this.length = rect.length;
	}
	/*
	 * Gets the location of a point
	 * @return returns position of a point
	 */
	public Point getLocation() {
		return new Point(this.x, this.y);
	}
	/*
	 * Sets the location of a point
	 * @param - location - point object holding location of a point
	 */
	public void setLocation(Point location) {
		this.x = location.x_axis;
		this.y = location.y_axis; 
	}
	/*
	 * Gets left  position of a rectangle
	 * @return returns position of a rectangle in X plane
	 */
	public float getLeft() { 
		return x; 
	}
	/*
	 * Gets Right position of rectangle
	 * @return - returns right position of a rectangle
	 */
	public float getRight() { 
		return x + breadth; 
	}
	/*
	 * Get Top of rectangle
	 * @return - returns top position of a rectangle
	 */
	public float getTop() { 
		return y; 
	}
	/*
	 * Get bottom of rectangle
	 * @returns returns bottom of a rectangle
	 */
	public float getBottom() { 
		return y + length; 
	}
	/*
	 * Get breadth of rectangle
	 * @return returns breadth of a rectangle
	 */
	public float getbreadth() { 
		return this.breadth; 
	}
	/*
	 * Get length of rectangle
	 * @returns returns length of a rectangle
	 */
	public float getLength() { 
		return this.length; 
	}
	/*
	 * Checks whether the points (X-Y position) is bounded by the rectangle or not
	 * @param p2 - location of a point
	 * @returns returns true if point is within the rectangle else return false
	 */
	public boolean contains(Point p2) {
		return this.x <= p2.x_axis && 
				p2.x_axis < this.x + this.breadth &&
				this.y <= p2.y_axis && 
				p2.y_axis < this.y + this.length;
	}
	/*
	 * Checks whether the rectangle is bounded by this rectangle or not
	 * @param r - node of a quad tree
	 * @returns returns true if the rectangle is within reference rectangle else return false
	 */
	public boolean contains(Rectangle r) {
		return (this.x <= r.getLeft()) && 
				((r.getLeft() + r.getbreadth()) <= (this.x + this.breadth)) && 
				(this.y <= r.getTop()) &&
				((r.getTop() + r.getLength()) <= (this.y + this.length)); 
	}
	/*
	 * Checks whether the rectangle is intersects this rectangle or not
	 * @param r - node of a quad tree
	 * @returns returns true if the rectangle is intersects reference rectangle else return false
	 */
	public boolean intersects(Rectangle r) { 
		return (r.getLeft() < this.x + this.breadth) && 
				(this.x < (r.getLeft() + r.getbreadth())) &&
				(r.getTop() < this.y + this.length) && 
				(this.y < r.getTop() + r.getLength());
	}
	/*
	 * Checks whether the rectangle is empty or not
	 * @returns True - if there is no rectangle i.e, length and breadth are zero otherwise return false
	 */
	public boolean isEmpty() {
		return (this.length <= 0 || this.breadth <= 0);
	}
	/*
	 * Returns the quadrant of the point w.r.t to reference rectangle region
	 * @param - rectangle object to identify the quadrant
	 * @return returns the quadrant namely 0 -> 1st; 1 -> 2nd; 2 -> 3rd and 3 -> 4th quadrants
	 */
	public int quadrant(Rectangle rectangle) {
		//System.out.println(pRect);
		int quadrant = -1;
		float midPointY = this.getLeft() + (this.getbreadth() / 2);
		float midPointX = this.getTop() + (this.getLength() / 2);

		// region above x reference
		boolean regionAboveXaxis = (rectangle.getTop() < midPointX && rectangle.getTop() + rectangle.getLength() < midPointX);
		// region below y reference
		boolean regionBelowXaxis = (rectangle.getTop() > midPointX);

		// Object can completely fit within the left quadrants
		if (rectangle.getLeft() < midPointY && rectangle.getLeft() + rectangle.getbreadth() < midPointY) {
			if (regionAboveXaxis) {
				quadrant = 1;
			}
			else if (regionBelowXaxis) {
				quadrant = 2;
			}
		}
		// Object can completely fit within the right quadrants
		else if (rectangle.getLeft() > midPointY) {
			if (regionAboveXaxis) {
				quadrant = 0;
			}
			else if (regionBelowXaxis) {
				quadrant = 3;
			}
		}
		return quadrant;
	}
	/*
	 * Returns string representation of rectangle object
	 * @returns String representation of rectangle 
	 */
	@Override
	public String toString() {
		return "Rectangle [x=" + x + ", y=" + y + ", breadth=" + breadth + ", length=" + length + " , quadrant=" + quadrant + "]";
	}
	/*
	* Gets the XY position of a rectangle
	* @returns location of a XY coordinates
	*/
	public String getPoint() {
		return getLeft() + ", " + getTop();
	}
}
