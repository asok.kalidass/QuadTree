import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TwoDimDictionaryTest {

	Point point;
	Rectangle rect = new Rectangle();
	TwoDimDictionary<Rectangle> twoDimDictionary;
	/**
	 * Initialization of this test class
	 */
	@Before
	public void setUp() {
		this.point = new Point();
		this.rect = new Rectangle();
		this.twoDimDictionary = new SimpleTwoDimDictionary<>(new Rectangle(0, 0, 474, 408));
	}

	@Test
	public void insertTest() throws Exception 
	{
		point.x_axis = 120;
		point.y_axis = 159;
		twoDimDictionary.insert(point);
		System.out.println("The Point that is inserted is :" + point.toString());
		assertTrue("The insert was success without any failure" , true);	
	}
	
	@Test
	public void queryTest() {
		List<Rectangle> rectangle = new ArrayList<>();		
		point.x_axis = 71;
		point.y_axis = 142;
		twoDimDictionary.insert(point);
		rect.breadth = 116;
		rect.length = 166;
		rect.x = 31;
		rect.y = 103;
		rectangle.addAll(twoDimDictionary.query(rect));
		rect = rectangle.get(0);
		assertEquals(point.x_axis, rect.getLeft(), 0f);
	}
	
	@Test
	public void countTest() {			
		point.x_axis = 71;
		point.y_axis = 142;
		twoDimDictionary.insert(point);
		rect.breadth = 116;
		rect.length = 166;
		rect.x = 31;
		rect.y = 103;
		int count = twoDimDictionary.count(rect);		
		assertEquals(1, count);
	}
	
	@Test
	public void sizeTest() {			
		point.x_axis = 71;
		point.y_axis = 142;
		twoDimDictionary.insert(point);
		point.x_axis = 150;
		point.y_axis = 142;
		twoDimDictionary.insert(point);
		point.x_axis = 389;
		point.y_axis = 29;
		twoDimDictionary.insert(point);
		point.x_axis = 3;
		point.y_axis = 2;
		twoDimDictionary.insert(point);
		rect.breadth = 474;
		rect.length = 408;
		rect.x = 0;
		rect.y = 0;
		int size = twoDimDictionary.count(rect);		
		assertEquals(4, size);
	}
}


