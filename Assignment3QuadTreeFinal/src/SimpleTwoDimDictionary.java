import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * This class Generates quad tree nodes of a quad tree and also with searching a node contents 
 *  Noteworthy Features: 
 * <ul>
 * <li>The rectangular region is recursively sub divided to four quadrants unit it reaches a preset depth
 * <li>Points in the region is searched recursively covering all the quadrants bounded by the reference rectangular region
 * <li>Dot file is generated for point in any of the location
 * <li>Recursive logic is used to generate the Dot File
 * </ul>
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <T> Generic type parameter to represent the nodes of a quad tree
 */
public class SimpleTwoDimDictionary<T> implements TwoDimDictionary<T> { 
	//Declarations
	int levels = 0;
	int loopCount = 1;
	public Rectangle bounds;
	public List<Rectangle> thisNodeContents = new ArrayList<>();
	List<SimpleTwoDimDictionary<Rectangle>> childNodes = new ArrayList<SimpleTwoDimDictionary<Rectangle>>(4);
	String refNode = "" ;
	/*
	 * Empty initialization of SimpleTwoDimDictionary 
	 */
	public SimpleTwoDimDictionary() {

	}
	/*
	 * Initializes the SimpleTwoDimDictionary with reference rectangular region
	 */
	public SimpleTwoDimDictionary(Rectangle bounds) {
		this.bounds = bounds;
	}
	/*
	 * Returns the state of quad tree contents
	 * @returns True if the nodes are zero else returns false
	 */
	public boolean IsEmpty() { 
		return bounds.isEmpty() || childNodes.size() == 0; 
	}
	/*
	 * Returns the nodes of a sub tree by visiting recursively to all the sub nodes
	 * @returns Rectangular points bounded by a region
	 */
	private List<Rectangle> childTreeContents() {
		ArrayList<Rectangle> queryResults = new ArrayList<>();
		//iterate and add all the sub nodes bounded by the query range
		for (SimpleTwoDimDictionary<Rectangle> child : childNodes) {
			queryResults.addAll(child.childTreeContents());
		}
		//add all the inserted points 
		queryResults.addAll(this.thisNodeContents);
		return queryResults;         
	}
	/* Search the region to get the inserted points
	 * (non-Javadoc)
	 * @see TwoDimDictionary#query(Rectangle)
	 * @param range - region to be searched
	 * @returns returns the points queried in the region 
	 */
	@SuppressWarnings("unchecked")
	public List<T> query(Rectangle range) {
		List<Rectangle> queryResults = new ArrayList<>();
		//check whether any inserted points falls within required range
		for (Rectangle item : thisNodeContents) {
			if (range.intersects(item)) {
				queryResults.add(item);
			}
		}
		//Similarly check for child contents 
		for (SimpleTwoDimDictionary<Rectangle> child : childNodes) {

			if (child.IsEmpty())
				continue;

			//Add the range contents if it bounded by any of the child contents
			if (child.bounds.contains(range))
			{
				queryResults.addAll(child.query(range));
				break;
			}

			//Add the child contents if it bounded by the range
			if (range.contains(child.bounds))
			{
				queryResults.addAll(child.childTreeContents());
				continue;
			}

			//At last, check for any intersected points and add it.
			if (child.bounds.intersects(range))
			{
				queryResults.addAll(child.query(range));
			}
		}
		return (List<T>) queryResults;
	}
	/*Inserts the points into the sub node 
	 * (non-Javadoc)
	 * @see TwoDimDictionary#insert(Point)
	 * @param point - data to be inserted into tree
	 */
	public void insert(Point point)
	{
		//if the point is not in the reference boundary don't create sub nodes
		if(!bounds.contains(point)) { 
			Preserve.isInserted = false;
			System.out.println("The region is outside the defined boundary"); 
			return;
		}
		//Split the node into 4 sub nodes
		if (childNodes.size() == 0)
			split();
		//if the point bounds any of the child nodes add them and recursively reach all the sub nodes. 
		for (SimpleTwoDimDictionary<Rectangle> child : childNodes) {
			if (child.bounds.contains(point))
			{
				Preserve.quadrant.add(child.bounds.quadrant);
				child.insert(point);
				return;
			}
		}
		//This is the final depth of the quad tree
		this.thisNodeContents.add(new Rectangle((point), 4f, 5f, 0));
		System.out.println("The Point is");
		System.out.print(new Rectangle((point), 4f, 5f, 0));
	}
	/*
	 * Creates the sub node of a quad tree until it reached the preset depth
	 */
	private void split()
	{
		// the smallest sub node (minimum depth ) 
		if ((bounds.getLength() * bounds.getbreadth()) <= 60) {
			return;
		}
		Preserve.isInserted = true;		
		//divide the region to create sub nodes
		float halfWidth = (bounds.getbreadth() / 2f);
		float halfHeight = (bounds.getLength() / 2f);
		//create the sub nodes like  first, second, third and fourth quadrants
		childNodes.add(new SimpleTwoDimDictionary<Rectangle>(new Rectangle(new Point(bounds.getLeft() + halfWidth, bounds.getTop()), halfWidth, halfHeight, 0)));
		childNodes.add(new SimpleTwoDimDictionary<Rectangle>(new Rectangle(bounds.getLocation(), halfWidth, halfHeight, 1)));
		childNodes.add(new SimpleTwoDimDictionary<Rectangle>(new Rectangle(new Point(bounds.getLeft(), bounds.getTop() + halfHeight), halfWidth, halfHeight, 2)));
		childNodes.add(new SimpleTwoDimDictionary<Rectangle>(new Rectangle(new Point(bounds.getLeft() + halfWidth, bounds.getTop() + halfHeight), halfWidth, halfHeight, 3)));		
		Preserve.setQuadTree(childNodes);	
	}
	/*
	 * Returns the number of nodes holding data in a quad tree
	 * (non-Javadoc)
	 * @see TwoDimDictionary#count(Rectangle)
	 * @param p - rectangular region to be searched to query the points
	 * @returns returns the number of pints in the region 
	 */
	@Override
	public int count(Rectangle p) {
		return query(p).size();

	}
	/*
	 * Returns the total size of the quad tree
	 * (non-Javadoc)
	 * @see TwoDimDictionary#size()
	 * @returns returns the size of the quad tree 
	 */
	@Override
	public int size() {
		//reference boundary to be queried
		Rectangle rect = new Rectangle(0, 0, 474, 408);
		return query(rect).size();
	}
	/*
	 * Generates dot file to visualize the quad tree through graphviz
	 * for more information please visit @link www.graphviz.com
	 */
	private void generateDotFile() {
		StringBuilder str = new StringBuilder();
		str.append("digraph quadtree {");
		str.append("\n");
		//generated level 1 of a quad tree
		str.append("A1 [shape=record,");	
		str.append("\n");
		str.append("label= \"{Rectangle:A1" + "|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");		
		str.append("\n");
		str.append(MessageFormat.format("A1:ne -> {0};", "B1")); //if point is in NE then map NE index
		str.append("\n");
		str.append(MessageFormat.format("A1:nw -> {0};", "C1"));
		str.append("\n");
		str.append(MessageFormat.format("A1:sw -> {0};", "D1"));
		str.append("\n");
		str.append(MessageFormat.format("A1:se -> {0};", "E1"));
		str.append("\n");
		//generate the level 2 of a quad tree
		switch (Preserve.quadrant.get(levels)) {
		case 0:
			levels++;
			str.append("B1 [shape=record, label=\"{Rectangle:B|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateNodesForLevels(str, "B1", "Y");
			break;
		case 1:
			levels++;
			str.append("C1 [shape=record, label=\"{Rectangle:C|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateNodesForLevels(str, "C1", "Y");
			break;
		case 2:
			levels++;
			str.append("D1 [shape=record, label=\"{Rectangle:D|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateNodesForLevels(str, "D1", "Y");
			break;
		case 3:
			levels++;
			str.append("E1 [shape=record, label=\"{Rectangle:E|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateNodesForLevels(str, "E1", "Y");				
			break;
		}
		generatePointNode(str, refNode);
		str.append("}");
		saveDotFile(str);
	}
	/*
	 * Generates level 2 of a quad tree in dot language
	 * @param str - reference to string builder instance
	 * @param ref - reference to map the region of one node to other
	 * @param mapping - reference to link each sub nodes of different levels
	 * @returns returns the string holding dot language representation of quad tree
	 */
	private String generateNodesForLevels(StringBuilder str, String ref, String mapping) {
		//Declarations
		int count = (int) Math.round(Math.random());	
		int initialCount = count;
		int depth = Preserve.quadrant.get(levels);
		String reference = "";
		//Generated four nodes for each of the levels
		//1th quadrant node
		str.append(MessageFormat.format("{0}:ne -> {1};", ref, mapping + count + count)); 
		str.append("\n");
		if(depth == 0) { reference = mapping + count + count; }
		count++;
		//2nd quadrant node
		str.append(MessageFormat.format("{0}:nw -> {1};", ref, mapping + count + count)); 
		str.append("\n");
		if(depth == 1) { reference = mapping + count + count; }
		count++;
		//3rd quadrant node
		str.append(MessageFormat.format("{0}:sw -> {1};", ref, mapping + count + count)); 
		str.append("\n");
		if(depth == 2) { reference = mapping + count + count; }
		count++;
		//4th quadrant node
		str.append(MessageFormat.format("{0}:se -> {1};", ref, mapping + count + count)); 
		str.append("\n");
		if(depth == 3) { reference = mapping + count + count; }
		count++;		
		count = initialCount;		
		refNode = reference;
		//Recursively generate the sub nodes for each levels of a tree
		switch (Preserve.quadrant.get(levels))
		{
		case 0:
			levels++;
			str.append(MessageFormat.format("{0} [shape=record, label=", reference));
			str.append("\"{Rectangle:J|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateSubNodes(str, reference, mapping, count);
			break;
		case 1:
			levels++;
			str.append(MessageFormat.format("{0} [shape=record, label=", reference));
			str.append("\"{Rectangle:k|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateSubNodes(str,  reference, mapping, count);
			break;
		case 2:
			levels++;
			str.append(MessageFormat.format("{0} [shape=record, label=", reference));
			str.append("\"{Rectangle:L|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateSubNodes(str,  reference, mapping, count);
			break;
		case 3:
			levels++;
			str.append(MessageFormat.format("{0} [shape=record, label=", reference));
			str.append("\"{Rectangle:M|{<ne>ne|<nw>nw|<sw>sw|<se>se}}\"];");
			str.append("\n");
			generateSubNodes(str,  reference, mapping, count);
			break;
		}		
		return str.toString();
	}
	/*
	 * Generate sub nodes of levels 3,4,5 and 6 of a quad tree
	 * @param str - reference to string builder instance
	 * @param ref - reference to map the region of one node to other
	 * @param mapping - reference to link each sub nodes of different levels
	 * @param count - to maintain the linkage points
	 */
	private void generateSubNodes(StringBuilder str, String ref, String mapping, int count) {
		if (loopCount == 1) {
			loopCount++;
			//generate the level nodes
			generateNodesForLevels(str, ref,"X");
			return;
		}
		else if (loopCount == 2) {
			loopCount++;
			generateNodesForLevels(str, ref,"W");	
			return;
		}
		else if (loopCount == 3) {
			loopCount++;
			generateNodesForLevels(str, ref,"V");
			return;
		}
		count++;
		str.append("\n");
	}
	/*
	 * Generates the point linkage of the external node
	 * @param srt - string builder instance
	 * @param ref - reference node to link this point node
	 */
	private void generatePointNode(StringBuilder str, String ref) {
		str.append("\n");
		str.append(ref);
		str.append(":");
		switch (Preserve.quadrant.get(5))
		{
		case 0:
			str.append("ne");
			break;
		case 1:
			str.append("nw");
			break;
		case 2:
			str.append("sw");
			break;
		case 3:
			str.append("se");
			break;
		}
		str.append(" ->");
		str.append(" <");
		str.append(Preserve.point.toString());
		str.append(">;");
	}
	/*
	 * Clears the class variables
	 */
	private void clear() {
		loopCount = 1;
		levels = 0;
		refNode = "";
	}
	/*
	 * Saves the created dot file in the project directory 
	 * @param str - string builder holding dot language strings
	 */
	private void saveDotFile(StringBuilder str) {

		try {
			FileWriter file = new FileWriter(System.currentTimeMillis() + "Graphviz.txt");
			BufferedWriter writer = new BufferedWriter(file);
			//flush the contents of string builder to the buffer writer 
			writer.write(str.toString());
			writer.close();			
			file.close();			
		}
		catch (Exception ex){
			System.out.println(ex.getMessage());
		}
	}
	/**
	 * This class preserves the state of a quad tree 	
	 */
	static class Preserve {
		//Declarations
		public static boolean isInserted = true;
		public static List<Integer> quadrant = new ArrayList<>();
		public static Point point = new Point();
		public static  List<List<SimpleTwoDimDictionary<Rectangle>>> quadTree = new ArrayList<List<SimpleTwoDimDictionary<Rectangle>>>();
		/*
		 * Get the quad tree
		 * @returns returns the quad tree 
		 */
		public static List<List<SimpleTwoDimDictionary<Rectangle>>> getQuadTree() {
			return quadTree;
		}
		/*
		 * Sets the quad tree
		 * @param childNodes - quad tree sub nodes of each level
		 */
		public static void setQuadTree(List<SimpleTwoDimDictionary<Rectangle>> childNodes) {
			quadTree.add(childNodes);
		}
	}
	/*
	 * Displays the contents of a quad tree
	 * (non-Javadoc)
	 * @see TwoDimDictionary#display()
	 */
	@Override
	public void display() {
		String indentation = " ";
		System.out.println(" ");
		for (List<SimpleTwoDimDictionary<Rectangle>> childNode : Preserve.quadTree) {
			//display the nodes 
			for(SimpleTwoDimDictionary<Rectangle> child : childNode) {
				System.out.print(indentation);
				System.out.println(child.bounds.getPoint());			
				indentation += " ";
			}
		}
		System.out.print(indentation);
		System.out.println(Preserve.point.toString());
	}
	/*
	 * Saves the quad tree in dot language to visualize the tree in graphviz
	 */
	public void saveDotFile() {
		clear();
		generateDotFile();
	}
}








