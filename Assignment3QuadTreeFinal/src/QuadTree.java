/*
 * This class represents the Quad Tree
 */
public class QuadTree {
	//denotes shape of nodes
	Rectangle bounds;
	//number of levels 
	int num;
	//inserted points
	Point point;
	//nodes
	QuadTree[] sub;
    /*
     * Empty initialization of quad tree object
     */
	public QuadTree() {
	}
	/*
	 * Initialize the quad tree with the supplied parameter
	 */
	public QuadTree(Rectangle bounds, int levels, QuadTree[] quadtree) {
		this.bounds = bounds;
		this.num = levels;
		this.point = bounds.getLocation();
		this.sub = quadtree;
	}    
}
