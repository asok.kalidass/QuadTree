import java.util.List;
/**
 * The TwoDimDictionary interface contains method to represent a quad tree in a XY plane
 * Implementation of quad tree has no limitation on the type i.e, sub nodes can be of any shape like circle, triangle etc,..
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <T> Data type of elements in the collection
 */
public interface TwoDimDictionary<T> {
	/*
	 * Inserts a point into quad tree
	 * @param point - point to be inserted
	 */
	public void insert(Point point);
	/*
	 * Search the quad tree to get the contents
	 * @returns returns the list of points queried in the region 
	 */
	public List<T> query(Rectangle range);
	/*
	 * Returns the number of points in the region 
	 * @param P - rectangle region to get the number of points
	 * @returns returns the number of points in the region
	 */
	public int count(Rectangle p);
	/*
	 * Returns the size of the quad tree
	 * @returns returns the size of the quad tree
	 */
	public int size();
	/*
	 * Displays the contents of the quad tree
	 */
	public void display();
	/*
	 * Displays the contents of the quad tree as dot file 
	 */
	public void saveDotFile();
}
