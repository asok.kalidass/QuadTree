import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.event.MouseMotionListener;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;

/**
 * This Generic class represents visual way of representing QuadTree in a XY plane
 * <p>
 * Assumptions/Restrictions: Dot file is generated in the project path immediately after each point insertion
 * <p>
 * Noteworthy Features: 
 * <ul>
 * <li>The quad tree depth is alterable by changing the depth area in the split method
 * <li>The rectangle is divided until the point sits in its least possible quadrant
 * <li>The generated dot file gives very good visual representation of the point in a quad tree 
 * <li>RIGHT CLICK and DRAG the area to get the number of points bounded by the rectangle
 * </ul>
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class VisualQuadTree extends JPanel {
	//Declarations 
	private static final long serialVersionUID = 1L;
	int count = 0;
	Rectangle rect = new Rectangle(0, 0, 474, 408);
	TwoDimDictionary<Rectangle> bounds = new SimpleTwoDimDictionary<>(new Rectangle(0, 0, 474, 408));
	List<Rectangle> queryedPoints = new ArrayList<>();
	int index[];
	Rectangle selectedLocation;
	java.awt.Point startPoint;
	boolean IsMouseDragged;
	JLabel lblMousePosition = new JLabel("mouseposition");

	/**
	 * Main method to invoke the swing UI to generate the visual representation of quadtree
	 * @param args command line parameters
	 */
	public static void main(String[] args) {
		new VisualQuadTree();
	}
	/**
	 * Loads Visual Quad Tree UI screen
	 */
	public VisualQuadTree() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception ex) {
				}
				JFrame frame = new JFrame("QuadTree");
				frame.setPreferredSize(new Dimension(500, 500));
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().add(new WorkPanel());
				frame.getContentPane().add(lblMousePosition, BorderLayout.SOUTH);
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}
	/*
	 * Represents work area of a quad tree UI screen
	 */
	public class WorkPanel extends JPanel {
		private static final long serialVersionUID = 1L;
        /**
         * Handle scenarios raised through work area and presents the information
         */
		public WorkPanel() {
			addMouseListener(new MouseAdapter() {				
				/**
				 * Handles mouse click events of work area
				 */
				@Override
				public void mouseClicked(MouseEvent e) {
					//on left mouse click insert the point in the quadrant and generate dot file
					if (SwingUtilities.isLeftMouseButton(e)) {
						java.awt.Point p = e.getPoint();
						Point point = new Point();
						point.x_axis = p.x;
						point.y_axis = p.y;
						SimpleTwoDimDictionary.Preserve.quadrant.clear();
						SimpleTwoDimDictionary.Preserve.point = null;
						//inserts the point
						bounds.insert(point);
						SimpleTwoDimDictionary.Preserve.point = point;						
						invalidate();
						//repaints the screen to denote the point in a quad tree
						repaint();
						List<Integer> qua = SimpleTwoDimDictionary.Preserve.quadrant;
						System.out.println("The Quadrants are ");
						for(int i : qua) {
							System.out.print(i);
						}
						//Generates dot file in the project folder in point is inserted
						if(SimpleTwoDimDictionary.Preserve.isInserted) {
							bounds.saveDotFile();
						}
					}													
				}
				/**
				 * Query's the number of points in the mouse dragged region 
				 */
				@Override
				public void mouseReleased(MouseEvent e) {
					IsMouseDragged = false;
					if (SwingUtilities.isRightMouseButton(e)) {
						if (queryedPoints.size() > 0) {	queryedPoints.clear(); }
                        //query the number of points in the dragged area							
						queryedPoints.addAll(bounds.query(selectedLocation));	
						System.out.println("The area to be queryed is: " + selectedLocation.toString());
						System.out.println("The number of points queryed is: " + queryedPoints.size());
						JOptionPane.showMessageDialog(null, "The number of points queryed is: " + queryedPoints.size());
					}
				}
				/**
				 * Set the mouse drag start position on mouse press
				 */
				@Override
				public void mousePressed(MouseEvent e) {
					startPoint = e.getPoint();
					IsMouseDragged = true;	
				}
			});
			
			addMouseMotionListener(new MouseMotionListener() {
                /**
                 * Gets the mouse position on mouse hover
                 */
				@Override
				public void mouseMoved(MouseEvent e) {
                    //sets the label on mouse pointer move
					lblMousePosition.setText(e.getPoint().toString());
				}
                /**
                 * Calculates the area on mouse drag
                 */
				@Override
				public void mouseDragged(MouseEvent e) {
					if(IsMouseDragged) {
						//calculates the dragged area
						selectedLocation = new Rectangle(
								(float) Math.min(e.getX(), startPoint.getX()),
								(float) Math.min(e.getY(), startPoint.getY()),
								(float) Math.max(e.getX(), startPoint.getX()),
								(float) Math.max(e.getY(), startPoint.getY()));						
					}
				}
			});
		}
        /**
         * Default UI screen dimension 
         */
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(600, 600);
		}
        /**
         * Repaint the UI after user action if needed
         * @param graphics - drawing related class
         */
		@Override
		protected void paintComponent(Graphics graphics) {			
			super.paintComponent(graphics); 
			Graphics2D g2d = (Graphics2D)graphics;
            //Draws the quad tree on every point that was inserted
			drawQuadTree(g2d);
		}
        /**
         * Draws the quad tree
         * @param g2d - 2D drawing related class
         */
		public void drawQuadTree(Graphics2D g2d) {			
			List<List<SimpleTwoDimDictionary<Rectangle>>> childNodes =  SimpleTwoDimDictionary.Preserve.getQuadTree();
			//draws the border
			if (count == 1) {
				Rectangle2D r2d = new Rectangle2D.Float(0, 0, 408, 474);
				g2d.draw(r2d);
			}
			for (List<SimpleTwoDimDictionary<Rectangle>> nodes : childNodes) {
				for (SimpleTwoDimDictionary<Rectangle> node : nodes) {

					//draws the inserted point 
					if(node.thisNodeContents != null) {
						for (Rectangle rectangle : node.thisNodeContents) {
							g2d.setColor(Color.RED);
							Ellipse2D circle = new Ellipse2D.Double(rectangle.x,rectangle.y, 3, 4);
							g2d.draw(circle);
							g2d.fill(circle);
						}
					}
					//draws quad tree
					g2d.setColor(Color.BLACK);
					Rectangle2D rect2D = new Rectangle2D.Float(node.bounds.x, node.bounds.y, node.bounds.breadth, node.bounds.length);
					g2d.draw(rect2D);                    
					System.out.println("(" + node.bounds.x + ", " + node.bounds.y + ", " + node.bounds.breadth + ", " + node.bounds.length + ", " + node.bounds.quadrant + ")");					
				}
			}
		}
	}
}

