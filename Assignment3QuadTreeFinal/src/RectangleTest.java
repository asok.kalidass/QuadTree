import static org.junit.Assert.*;
import org.junit.Test;
/**
 * Represent JUNIT test class of Rectangle
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class RectangleTest {
	//Reference Boundary 
	Rectangle rectangle = new Rectangle(0, 0, 474, 408);
	/*
	 * A test to find the points belong to first quadrant of a boundary or not
	 */
	@Test
	public void firstQuadrantTest() {
		Point point = new Point(279f, 128f);		
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		int quadrant = rectangle.quadrant(rect);
		assertEquals(0, quadrant);
	}
	/*
	 * A test to find the points belong to second quadrant of a boundary or not
	 */
	@Test
	public void secondQuadrantTest() {
		Point point = new Point(104f, 108f);		
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		int quadrant = rectangle.quadrant(rect);
		assertEquals(1, quadrant);
	}
	/*
	 * A test to find the points belong to third quadrant of a boundary or not
	 */
	@Test
	public void thirdQuadrantTest() {
		Point point = new Point(114f, 308f);		
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		int quadrant = rectangle.quadrant(rect);
		assertEquals(2, quadrant);
	}
	/*
	 * A test to find the points belong to fourth quadrant of a boundary or not
	 */
	@Test
	public void fourthQuadrantTest() {
		Point point = new Point(314f, 308f);		
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		int quadrant = rectangle.quadrant(rect);
		assertEquals(3, quadrant);
	}
	/*
	 * A test to check whether the rectangle is located in the reference rectangle or not
	 */
	@Test
	public void containsRectangleTest() {
		Point point = new Point(314f, 308f);		
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		boolean IsBounded = rectangle.contains(rect);
		assertEquals(true, IsBounded);
	}
	/*
	 * A test to check whether the rectangle is located in the reference rectangle or not
	 */
	@Test
	public void notContainsRectangleTest() {
		Point point = new Point(600f, 600f);		
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		boolean IsBounded = rectangle.contains(rect);
		assertEquals(false, IsBounded);
	}
	/*
	 * A test to check whether the point is located in the reference rectangle or not
	 */
	@Test
	public void containsPointTest() {
		Point point = new Point(314f, 308f);				
		boolean IsBounded = rectangle.contains(point);
		assertEquals(true, IsBounded);
	}
	/*
	 * A test to check whether the point is located in the reference rectangle or not
	 */
	@Test
	public void notContainsTest() {
		Point point = new Point(600f, 600f);		
		boolean IsBounded = rectangle.contains(point);
		assertEquals(false, IsBounded);
	}
	/*
	 * A test to check whether the rectangle is intersected by the reference rectangle or not
	 */
	@Test
	public void notIntersectsTest() {
		Point point = new Point(600f, 600f);
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		boolean IsBounded = rectangle.intersects(rect);
		assertEquals(false, IsBounded);
	}
	/*
	 * A test to check whether the rectangle is intersected by the reference rectangle or not
	 */
	@Test
	public void intersectsTest() {
		Point point = new Point(300f, 300f);
		Rectangle rect = new Rectangle(point, 45f, 46f, 0);
		boolean IsBounded = rectangle.intersects(rect);
		assertEquals(true, IsBounded);
	}	
}
