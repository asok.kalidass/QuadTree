import static org.junit.Assert.*;
import org.junit.Test;
/**
 * Represent JUNIT test class of QuadTree
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class QuadTreeTest {
	/*
	 * A test to verify the constructor initialization
	 */
	@Test
	public void quadTreesTest() {
		Point point = new Point();
		point.x_axis = 134;
		point.y_axis = 45;
		Rectangle rect = new Rectangle(point, 146f, 43f, 0);
		QuadTree quadtree = new QuadTree(rect, 1, null);
		assertEquals(rect.breadth, quadtree.bounds.getbreadth(), 0f);
	}
}
