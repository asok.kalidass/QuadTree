import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
/**
 * Represent JUNIT test class of SimpleTwoDimDictionary 
 * @author asok
 *@author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 */
public class SimpleTwoDimDictionaryTest {
    //Declarations
	TwoDimDictionary<Rectangle> bounds = new SimpleTwoDimDictionary<>(new Rectangle(0, 0, 474, 408));
	Point point = new Point();;
	Rectangle rect = new Rectangle();
	/*
	 * A test to verify the insert functionality	
	 */
	@Test
	public void insertTest() {
		point.x_axis = 71;
		point.y_axis = 142;
		bounds.insert(point);
		System.out.println("The Point that is inserted is :" + point.toString());
		assertTrue("The insert was success without any failure" , true);		
	}
	/*
	 * A test to verify the querying of points functionality	
	 */
	@Test
	public void queryTest() {
		List<Rectangle> rectangle = new ArrayList<>();		
		point.x_axis = 71;
		point.y_axis = 142;
		bounds.insert(point);
		rect.breadth = 116;
		rect.length = 166;
		rect.x = 31;
		rect.y = 103;
		rectangle.addAll(bounds.query(rect));
		rect = rectangle.get(0);
		assertEquals(point.x_axis, rect.getLeft(), 0f);
	}
	/*
	 * A test to verify the insert and querying functionality through random way
	 */
	@Test
	public void randomInsertAndQueryTest() {
		//random Insert
		List<Rectangle> rectangles = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			int x = (int) Math.random();
			int y = (int) Math.random();
			Point point = new Point(x, y);
			bounds.insert(point);
		}
		System.out.println("Done inserting " + 10 + " points");
		//range searches
		for (int i = 0; i < 10; i++) {        	
			int x = (int) Math.random();
			int y = (int) Math.random();
			Point point = new Point(x, y);
			Rectangle rectangle = new Rectangle(point, 400f, 400f, 0);
			rectangles.addAll(bounds.query(rectangle));
			for(Rectangle rect : rectangles) {
				System.out.println("The random inserted points are");
				System.out.println(rect.toString());
			}
			assertTrue("Cannot be compared as points inserted are random. The output is printed in console" , true);	
		}
	}
	/*
	 * A test to verify the insert functionality outside the boundary	
	 */
	@Test
	public void outsideBoundaryInsertAndQueryTest() {
		List<Rectangle> rectangle = new ArrayList<>();		
		point.x_axis = 500;
		point.y_axis = 500;
		bounds.insert(point);
		rect.breadth = 116;
		rect.length = 166;
		rect.x = 31;
		rect.y = 103;
		rectangle.addAll(bounds.query(rect));
		assertEquals(0, rectangle.size());
	}
	/*
	 * A test to generate the dot file	
	 */
	@Test
	public void saveDotFileTest() {
		SimpleTwoDimDictionary<Rectangle> rect = new SimpleTwoDimDictionary<>();
		point.x_axis = 71;
		point.y_axis = 142;
		bounds.insert(point);
		SimpleTwoDimDictionary.Preserve.point = point;
		rect.saveDotFile();
		assertTrue("The file is generated in the project path" , true);	
	}
	/*
	 * A test to generate the dot file for a point that was not inserted	
	 */
	@Test
	public void outsideBoundarySaveDotFileTest() {
		point.x_axis = 500;
		point.y_axis = 600;
		bounds.insert(point);
		SimpleTwoDimDictionary.Preserve.point = point;
		assertTrue("The file shouldnt be generated in the project path" , true);	
	}
	/*
	 * A test to verify the number of points in the query range
	 */
	@Test
	public void countTest() {			
		point.x_axis = 71;
		point.y_axis = 142;
		bounds.insert(point);		
		rect.breadth = 116;
		rect.length = 166;
		rect.x = 31;
		rect.y = 103;
		int count = bounds.count(rect);		
		assertEquals(1, count);
	}
	/*
	 * A test to verify the number of points in the boundary
	 */
	@Test
	public void sizeTest() {			
		point.x_axis = 71;
		point.y_axis = 142;
		bounds.insert(point);
		point.x_axis = 150;
		point.y_axis = 142;
		bounds.insert(point);
		point.x_axis = 389;
		point.y_axis = 29;
		bounds.insert(point);
		point.x_axis = 3;
		point.y_axis = 2;
		bounds.insert(point);
		rect.breadth = 474;
		rect.length = 408;
		rect.x = 0;
		rect.y = 0;
		int size = bounds.count(rect);		
		assertEquals(4, size);
	}
	/*
	 * A test to display the quad tree
	 */
	@Test
	public void display() {
		point.x_axis = 71;
		point.y_axis = 142;
		bounds.insert(point);
		SimpleTwoDimDictionary.Preserve.point = point;	
		bounds.display();
		assertTrue("Quad Tree nodes are printed in the ecllipse console window" , true);	
	}
}
