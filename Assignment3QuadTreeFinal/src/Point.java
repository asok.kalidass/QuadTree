/**
 * This class represents X and Y position of a point in  an XY plane
 *
 */
public class Point {
//Declarations
float x_axis, y_axis;
/*
 * Empty initialization of Point object
 */
public Point() {
	
}
/*
 * Initialization of point object
 * @param x - position of a point in X axis
 * @param y - position of a point in Y axis
 */
public Point(float x, float y) {
	this.x_axis = x;
	this.y_axis = y;
}
/*
 * Gets the X axis
 *  @return - returns the position of a point in X plane
 */
public float getX_axis() {
	return x_axis;
}
/*
 * Sets the x axis
 * @param x_axis - position of a point in X axis
 */
public void setX_axis(float x_axis) {
	this.x_axis = x_axis;
}
/*
 * Gets the Y axis
 * @return - returns the position of a point in Y plane
 */
public float getY_axis() {
	return y_axis;
}
/*
 * Sets the y axis
 * @param y_axis - position of a point in Y axis
 */
public void setY_axis(float y_axis) {
	this.y_axis = y_axis;
}
/*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 * String representation of Point object
 * @return - X and Y positions of a string
 */
@Override
public String toString() {
	return x_axis + ", " + y_axis;
}
}
