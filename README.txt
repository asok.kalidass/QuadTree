      Pre Requisite:

   Swing jar files are needed to run this program. Please, addreference to 'jgoodies-forms-1.8.0-sources' if not compiling
     
Application:

   The working version of the application is placed in the folder UI. Just click on it to see the demo and generated dot file.
QuadTree:

   Description: A program to implement the quadtree in Java

   Files: 
     Point.java
     QuadTree.java
     QuadTreeTest.java
     Rectangle.java
     RectangleTest.java
     SimpleTwoDimDictionary.java
     SimpleTwoDimDictionaryTest.java
     TwoDimDictionary.java
     TwoDimDictionaryTest.java
     VisialQuadTree.java

   Run test instructions: 
     TestClasses:

     QuadTreeTest.java
     RectangleTest.java
     SimpleTwoDimDictionaryTest.java
     TwoDimDictionaryTest.java

     GUI:

     VisualQuadTree.java

   Features:
   
   	- The rectangular region is recursively sub divided to four quadrants unit it reaches a pre-set depth.
   
   	- Points in the region is searched recursively covering all the quadrants bounded by the reference rectangular region.
 
   	- Dot file is generated for a point in any of the location.
 
   	- Tested through JUNIT test framework. 

   	- Java Doc is included.

   Extra Work:

        - GUI is created (VisualQuadTree.java) to demonstate insert, Query and Dot file generation.

   GUI:
        - Left click to insert a point in a region.
 
        - Look for the generated to dot file in the project folder to visualize quadtree through graphvis.
   
        - Right click and drag the area to query the number of points in the dragged region.
   